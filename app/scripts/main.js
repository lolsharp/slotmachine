

var coins = 3;
var lifes = 2;
var running = false;

var start = new Audio("assets/smb2_bonus_chance_start.wav");
var lose = new Audio("assets/smb2_bonus_chance_lose.wav");
var win = new Audio("assets/smb2_bonus_chance_win.wav");

 
var Mediator = (function () {

    var channels = {};

    //
    // Subscribe to a channel
    //
    function subscribe(channel, fn, that) {
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({
            context: that,
            callback: fn
        });
    }

    //
    // Publish a channel (like triggering an event)
    //
    function publish(channel) {

        if (!channels[channel]) {
            return false;
        }
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0, l = channels[channel].length; i < l; i += 1) {
            var subscription = channels[channel][i];
            if (typeof subscription.context === undefined) {
                subscription.callback(args);
            } else {
                subscription.callback.call(subscription.context, args);
            }
        }
    }

    return {
        subscribe: subscribe,
        publish: publish
    }

})();

var BackendConnector = (function () {
    function send() {
        $.ajax({
            url: "http://dragonquest.at/numbers.php?jsonp",
            dataType: 'jsonp',
            success: function (data) {
                Mediator.publish('datareceived',data);
            },
            error: function (err) {

            }
        });
    }

    Mediator.subscribe('inputGiven', send);

})();

$('html').on('mousedown', function () {
    if(!running) {
        running = true;
        Mediator.publish('inputGiven');
    }
});

$('html').on('keydown', function () {
    if(!running) {
        running = true;
        Mediator.publish('inputGiven');
    }
});


window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();

Mediator.subscribe('datareceived', function (data) {
    start.play();
    coins--;
    $('#coins').text(coins);
    var toplay;
    if(data[0].result > 0) {
        toplay = win;
    } else
    {
        toplay = lose;
    }

    coins = coins + data[0].result;

	requestAnimFrame(function() {
		$('#slot0').removeClass('animate');
		$('#slot1').removeClass('animate');
		$('#slot2').removeClass('animate');
		requestAnimFrame(function() {
			$('#slot0').addClass('animate');
			$('#slot1').addClass('animate');
			$('#slot2').addClass('animate');
		});
	});


    
   
    for (var i = 0; i < 3; i++) {

    	$('#slot'+ i).removeClass('slot-cherry');
		$('#slot'+ i).removeClass('slot-enemy');
		$('#slot'+ i).removeClass('slot-vegy');
		$('#slot'+ i).removeClass('slot-star');

		$('#slot2').on('webkitAnimationEnd', function () {
            toplay.play();
			if(coins > 100) {
                lifes++;
                coins -= 100;
            }
            if(coins <= 0) {
                lifes--;
                coins = 5;
            }
            $('#coins').text(coins);
            $('#lifes').text(lifes);
            running = false;
		});

    	switch(data[0].slots[i]) {
    		case 0: $('#slot'+ i).addClass('slot-cherry');
    				break;
			case 1: $('#slot'+i).addClass('slot-enemy');
					break;
			case 2: $('#slot'+i).addClass('slot-vegy');
					break;
			case 3: $('#slot'+i).addClass('slot-star');
					break;
    	}
    };

});

	